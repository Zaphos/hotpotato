# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added
- Automated migration testing from [@rhysmd](https://gitlab.com/rhysmd)
- Debug sender in testing environment from [@zacps](https://gitlab.com/zacps)
- New CI coverage report from [@zacps](https://gitlab.com/zacps)
- New API tests from [@rhysmd](https://gitlab.com/rhysmd)
- Add a database creation cli command and stamp database with alembic revision on creation from [@rhysmd](https://gitlab.com/rhysmd)
- Add more context to custom message notifications [@zacps](https://gitlab.com/zacps)

### Changed
- Replace ModelObject with SQLAlchemy single table inheritance from [@rhysmd](https://gitlab.com/rhysmd)
- Re-factor Docker set-up to simplify maintenance from [@rhysmd](https://gitlab.com/rhysmd)
- Re-factor API notification handling from [@callum027](https://gitlab.com/Callum027)

### Fixed
- Fix up issues in test data generation from [@callum027](https://gitlab.com/Callum027)
- Fix output of Modica notification status function from [@rhysmd](https://gitlab.com/rhysmd)
- Fix errors display when no user is currently on pager from [@rhysmd](https://gitlab.com/rhysmd)
