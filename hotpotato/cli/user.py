"""
Command line interface for adding and removing users.
"""


import click
import flask
import flask.cli
import pytz

from hotpotato import roles, users
from hotpotato.models import db, user_datastore


@click.group("user", cls=flask.cli.AppGroup)
def user():
    """
    User commands.
    """

    pass


@user.command("create")
@click.argument("name", type=click.STRING)
@click.argument("email", type=click.STRING)
@click.option("-t", "--timezone", default="UTC")
@click.option("-a", "--active", default=True, is_flag=True)
@click.password_option()
def create(name, email, password, timezone, active):
    """
    Create a user.
    """

    if timezone not in pytz.all_timezones:
        raise click.UsageError("Unknown timezone")

    new_user = users.create(email, password, name, timezone, active)

    role_admin = roles.get_by_name("admin")
    new_user.add_role(role_admin)
    db.session.commit()


@user.command("deactivate")
@click.argument("email", type=click.STRING)
def deactivate(email):
    """
    Deactivate a user.
    """
    try:
        user = users.get_by_email(email)

        if user_datastore.deactivate_user(user):
            user_datastore.commit()
        else:
            click.echo("User {} was already deactivated.".format(user.email))
    except users.UserEmailError:
        raise click.UsageError("User does not exist")


@user.command("activate")
@click.argument("email", type=click.STRING)
def activate(email):
    """
    Activate a user.
    """
    try:
        user = users.get_by_email(email)

        click.echo(user.email)
        click.echo(user.active)

        if user_datastore.activate_user(user):
            click.echo(user.active)
            user_datastore.commit()
            click.echo(user.active)
        else:
            click.echo("User {} was already activated.".format(user.email))
    except users.UserEmailError:
        raise click.UsageError("User does not exist")
