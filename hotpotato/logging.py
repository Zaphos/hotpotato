"""
Logging functions.
"""


import logging

from flask.logging import default_handler as flask_default_handler


def init_app(app):
    """
    Initialise the logging system.
    """

    formatter = logging.Formatter("%(asctime)s %(name)s: [%(levelname)s] %(message)s")

    app.logger.removeHandler(flask_default_handler)

    app.logger.setLevel(app.config["HOTPOTATO_LOG_LEVEL"])

    stream_handler = logging.StreamHandler()
    stream_handler.setFormatter(formatter)
    app.logger.addHandler(stream_handler)

    app.logger.debug("Logger started.")
