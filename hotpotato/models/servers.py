"""
Server model classes.
"""
import pytz
from sqlalchemy.orm import validates

from hotpotato.models.database import db


class Server(db.Model):
    """
    Monitoring servers that send notifications to Hot Potato.
    """

    __tablename__ = "servers"

    id = db.Column(db.Integer, primary_key=True)
    hostname = db.Column(db.Text, nullable=True)
    apikey = db.Column(db.Text, nullable=True)
    enable_heartbeat = db.Column(db.Boolean)
    disable_missed_heartbeat_notif = db.Column(db.Boolean)
    missed_heartbeat_limit = db.Column(db.Integer)
    timezone = db.Column(db.Text, nullable=True)
    link = db.Column(db.Text, nullable=True)

    @validates("timezone")
    def validate_timezone(self, key, timezone):
        """
        Check timezone is a valid pytz timezone.
        """

        if timezone not in pytz.all_timezones:
            from hotpotato.servers import ServerTimezoneError

            raise ServerTimezoneError(self.id, timezone)

        return timezone

    def timezone_get_as_timezone(self):
        """
        Return this server's timezone string as a timezone object.
        """

        return pytz.timezone(self.timezone)

    def __repr__(self):
        return (
            "ID: "
            + str(self.id)
            + "Hostname: "
            + str(self.hostname)
            + "API Key: "
            + str(self.apikey)
        )
