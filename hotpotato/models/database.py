"""
Flask SQLAlchemy setup.
"""
import flask_sqlalchemy

db = flask_sqlalchemy.SQLAlchemy()
