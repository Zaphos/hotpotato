"""
Heartbeat unit test functions.
"""


import random
from datetime import timedelta

from hotpotato import models


def create_fakes(fake, server, min_days=6, max_days=7):
    """
    Generate a generator of fake heartbeats for the given server, with randomly set data.
    """

    # TODO: Make hbeat_freq configurable.
    hbeat_freq = 60
    num = random.randint(
        int(timedelta(days=min_days).total_seconds()) // hbeat_freq,
        int(timedelta(days=max_days).total_seconds()) // hbeat_freq,
    )

    return (
        models.Heartbeat(
            server_id=server.id,
            received_dt=fake.past_datetime(start_date="-{}d".format(max_days)),
        )
        for _ in range(num)
    )
