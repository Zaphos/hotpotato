from hotpotato import __version__


def test_version(runner):
    """
    Test version runs without any errors.
    """
    result = runner.invoke(args=["hotpotato", "version"])
    assert __version__ in result.output
    assert result.exit_code is 0


def test_config(runner):
    """
    Test config runs without any errors.
    """
    result = runner.invoke(args=["hotpotato", "config"])
    assert result.exit_code is 0
