"""
On-call contact functions.
"""


from hotpotato import notifications
from hotpotato.models import OncallContact as Model, db

METHOD = frozenset(
    (
        # "app",
        "sms",
        "pager",
        "pushover",
    )
)


class OncallContactError(Exception):
    """
    On-call contact exception base class.
    """

    pass


class OncallContactIDError(OncallContactError):
    """
    Exception for invalid on-call contact ID.
    """

    def __init__(self, oncall_contact_id):
        self.oncall_contact_id = oncall_contact_id
        super().__init__(
            "Invalid on-call contact ID (or unable to find on-call contact with ID): {}".format(
                oncall_contact_id
            )
        )


class OncallContactMethodError(OncallContactError):
    """
    Exception for invalid on-call contact method.
    """

    def __init__(self, method):
        super().__init__(
            "Invalid Method: {} Expected one of [{}]".format(method, ", ".join(METHOD))
        )


class OncallContactContactError(OncallContactError):
    """
    On-call contact data exception base class.
    """

    pass


class OncallContactContactNotFoundError(OncallContactContactError):
    """
    Exception for when on-call contact data was not found.
    """

    def __init__(self, method, contact):
        self.method = method
        self.contact = contact
        super().__init__(
            "Unable to find on-call contact with data: {} (method {})".format(
                contact, method
            )
        )


def get(oncall_contact_id):
    """
    Get an on-call contact object using the given ID, and return it.
    """

    oncall_contact = Model.query.filter_by(id=oncall_contact_id).first()

    if not oncall_contact:
        raise OncallContactIDError(oncall_contact_id)

    return oncall_contact


def get_by(
    user_id=None,
    method=None,
    verifiable=None,
    priority=None,
    contact=None,
    send_pages=None,
    send_failures=None,
):
    """
    Return a generator of on-call objects found with the given search parameters,
    ordered by verifiable priority, then unverifiable priority.
    """

    query = Model.query
    results = []

    if user_id is not None:
        query = query.filter_by(user_id=user_id)
    if method is not None:
        if method not in METHOD:
            raise OncallContactMethodError(method)
        query = query.filter_by(method=method)
    if verifiable is not None:
        query = query.filter_by(verifiable=verifiable)
    if priority is not None:
        query = query.filter_by(priority=priority)
    if contact is not None:
        query = query.filter_by(contact=contact)
    if send_pages is not None:
        query = query.filter_by(send_pages=send_pages)
    if send_failures is not None:
        query = query.filter_by(send_failures=send_failures)

    if verifiable is None or verifiable is True:
        results.extend(
            query.filter_by(verifiable=True).order_by(Model.priority.asc()).all()
        )
    if verifiable is None or verifiable is False:
        results.extend(
            query.filter_by(verifiable=False).order_by(Model.priority.asc()).all()
        )

    return tuple(results)


def get_for_user(user, verifiable=None):
    """
    Return a generator for all on-call contact methods for the given user,
    ordered by verifiable priority, then unverifiable priority.
    """

    return get_for_user_id(user.id, verifiable=verifiable)


def get_for_user_id(user_id, verifiable=None):
    """
    Return a generator for all on-call contact methods for the given user ID,
    ordered by verifiable priority, then unverifiable priority.
    """

    return get_by(user_id=user_id, verifiable=verifiable)


def exists(*args, **kwargs):
    """
    Return True if there is any on-call contacts which match the given search parameters.
    Takes the same search parameters as get_by.
    """

    return True if tuple(get_by(*args, **kwargs)) else False


def create(user_id, method, contact, priority, send_pages=False, send_failures=False):
    """
    Create an on-call contact object using the given input data,
    add it to the database, and return it.
    """

    if method not in METHOD:
        raise OncallContactMethodError(method)

    verifiable = method_is_verifiable(method)

    oncall_contact = Model(
        user_id=user_id,
        method=method,
        verifiable=verifiable,
        priority=priority,
        contact=contact,
        send_pages=send_pages,
        send_failures=send_failures,
    )

    db.session.add(oncall_contact)
    db.session.commit()

    return oncall_contact


def delete(oncall_contact):
    """
    Delete the given on-call contact from the database, and return it.
    """

    db.session.delete(oncall_contact)
    db.session.commit()

    return oncall_contact


def delete_by_id(oncall_contact_id):
    """
    Delete the on-call contact with the given ID from the database, and return it.
    """

    return delete(get(oncall_contact_id))


def method_is_verifiable(method):
    """
    Return True if the given on-call contact method is verifiable.
    """

    return method in notifications.VERIFIABLE_METHOD
