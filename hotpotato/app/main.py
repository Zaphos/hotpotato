"""
Hot Potato main Flask app. This is the one that is used in a standard Hot Potato instance.
"""


from hotpotato import app as hotpotato_app

app = hotpotato_app.create()
