"""
Hot Potato configuration.
"""


import configparser
import os
import pathlib
from collections import namedtuple

from flask import current_app

FILE_EXT = "ini"

BOOLEAN_VALUES = {
    "1": True,
    "yes": True,
    "true": True,
    "on": True,
    "0": False,
    "no": False,
    "false": False,
    "off": False,
}


class ConfigError(Exception):
    """
    Hot Potato configuration error.
    """

    pass


def init_app(
    app, preset=None, filepaths=None, use_defaults=True, setup_server_name=False
):
    """
    Initialise the given Flask app with the Hot Potato configuration.
    """

    # Set up the configuration parser, available sections, and parsing methods.
    config = configparser.ConfigParser(strict=True)

    config.add_section("flask")
    config.add_section("hotpotato")
    config.add_section("security")
    config.add_section("mail")
    config.add_section("sqlalchemy")
    config.add_section("rabbitmq")
    config.add_section("modica")
    config.add_section("twilio")
    config.add_section("app")
    config.add_section("sms")
    config.add_section("pager")
    config.add_section("pushover")

    g = namedtuple("g", ["file", "env"])
    gstr = g(config.get, str)
    gbool = g(config.getboolean, _bool_get)
    gint = g(config.getint, int)

    # Create the ordered list of configuration sources, starting from the preset config,
    # and read from the configuration sources into the main config parser.
    # Environment variables are checked later, when values are actually read.
    config_sources = [preset] if preset else []

    if use_defaults:
        sources = _source_read("/etc/hotpotato/conf.d", default=True)
        if sources:
            config_sources.extend(sources)

        if not sources:
            sources = _source_read("config.{}".format(FILE_EXT), default=True)
            if sources:
                config_sources.extend(sources)

    if filepaths:
        for fp in filepaths:
            sources = _source_read(fp, required=True)
            if sources:
                config_sources.extend(sources)

    for source in config_sources:
        for section_name, section in source.items():
            if section_name in config:
                for key, value in section.items():
                    config.set(section_name.lower(), key, value)

    # Parse the loaded configuration and read from environment variables (highest priority),
    # and set up the app with the parameters.
    app.config.update(
        [
            _get(gbool, "flask", "testing", default=False),
            _get(gstr, "flask", "secret_key", required=True),
            _get(gstr, "hotpotato", "webui_url", required=setup_server_name),
            _get(gstr, "hotpotato", "troublecode_url", required=True),
            _get(
                gstr,
                "hotpotato",
                "log_level",
                default="DEBUG"
                if "FLASK_DEBUG" in app.config and app.config["FLASK_DEBUG"]
                else "WARNING",
            ),
            _get(gstr, "security", "password_hash", default="bcrypt"),
            _get(gstr, "security", "password_salt", required=True),
            _get(gbool, "security", "changeable", default=True),
            _get(gbool, "security", "confirmable", default=False),
            _get(gbool, "security", "recoverable", default=True),
            _get(gbool, "security", "trackable", default=True),
            _get(gbool, "mail", "suppress_send", default=False),
            _get(gstr, "mail", "default_sender", default=None),
            _get(gstr, "cockroach", "server", required=True),
            _get(gint, "cockroach", "port", default=26257),
            _get(gstr, "cockroach", "database", required=True),
            _get(gstr, "cockroach", "username", default=None),
            _get(gstr, "cockroach", "password", default=None),
            _get(gstr, "rabbitmq", "server", required=True),
            _get(gint, "rabbitmq", "port", default=5672),
            _get(gbool, "rabbitmq", "use_ssl", default=False),
            _get(gstr, "rabbitmq", "username", required=True),
            _get(gstr, "rabbitmq", "password", required=True),
            _get(gstr, "rabbitmq", "vhost", default="/"),
            _get(gbool, "app", "enabled", default=False),
            _get(gbool, "pager", "enabled", default=False),
            _get(gbool, "sms", "enabled", default=False),
            _get(gbool, "pushover", "enabled", default=False),
            _get(gbool, "modica", "enabled", default=False),
            _get(gbool, "twilio", "enabled", default=False),
        ]
    )

    # Load required mail settings if sending is enabled.
    if not app.config["MAIL_SUPPRESS_SEND"] and app.config["MAIL_DEFAULT_SENDER"]:
        app.config.update(
            [
                _get(gstr, "mail", "server", required=True),
                _get(gint, "mail", "port", default=587),
                _get(gbool, "mail", "use_ssl", default=True),
                _get(gstr, "mail", "username", required=True),
                _get(gstr, "mail", "password", required=True),
            ]
        )

    # Determine correct CockroachDB database URI for SQLAlchemy.
    if app.config["COCKROACH_USERNAME"]:
        app.config["SQLALCHEMY_DATABASE_URI"] = "cockroachdb://{}:{}@{}:{}/{}".format(
            app.config["COCKROACH_USERNAME"],
            app.config["COCKROACH_PASSWORD"],
            app.config["COCKROACH_SERVER"],
            app.config["COCKROACH_PORT"],
            app.config["COCKROACH_DATABASE"],
        )
    else:
        app.config["SQLALCHEMY_DATABASE_URI"] = "cockroachdb://{}:{}/{}".format(
            app.config["COCKROACH_SERVER"],
            app.config["COCKROACH_PORT"],
            app.config["COCKROACH_DATABASE"],
        )

    # Determine the preferred URL scheme.
    app.config.update(
        [
            _get(
                gstr,
                "flask",
                "preferred_url_scheme",
                default="http" if app.config["TESTING"] else "https",
            )
        ]
    )

    # Load required Pushover settings if Pushover is enabled.
    if app.config["PUSHOVER_ENABLED"]:
        app.config.update(
            [
                _get(gstr, "pushover", "api_token", required=not app.config["TESTING"]),
                _get(gint, "pushover", "alert_priority", default=2),
                _get(gint, "pushover", "message_priority", default=0),
                _get(gint, "pushover", "handover_priority", default=0),
                # How long and ofter to retry for emergency priority 2 alerts
                # default to every 2 minutes for 3 hours
                _get(gint, "pushover", "emergency_retry", default=120),
                _get(gint, "pushover", "emergency_expiry", default=10800),
            ]
        )

    # Load required Modica settings if Modica is enabled.
    if app.config["MODICA_ENABLED"]:
        app.config.update(
            [
                _get(gstr, "modica", "url", required=not app.config["TESTING"]),
                _get(gstr, "modica", "username", required=not app.config["TESTING"]),
                _get(gstr, "modica", "password", required=not app.config["TESTING"]),
            ]
        )

    # Load required Twilio settings if Twilio is enabled.
    if app.config["TWILIO_ENABLED"]:
        app.config.update(
            [
                _get(gstr, "twilio", "account_sid", required=not app.config["TESTING"]),
                _get(gstr, "twilio", "auth_token", required=not app.config["TESTING"]),
                _get(gstr, "twilio", "sms_number", required=not app.config["TESTING"]),
            ]
        )

    if setup_server_name:
        app.config["SERVER_NAME"] = app.config["HOTPOTATO_WEBUI_URL"]


def _source_read(filepath, required=False, default=False, recurse=False):
    """
    Go through the given filepath to read all valid configuration sources,
    and return all of them.
    """

    path = pathlib.Path(filepath)

    if path.is_dir():
        sources = []
        for subpath in path.iterdir():
            if (subpath.is_dir() and recurse) or subpath.is_file:
                sources.append(
                    _source_read(
                        str(subpath),
                        required=required,
                        default=default,
                        recurse=recurse,
                    )
                )
        return tuple(sources)

    else:
        try:
            # Base case of recursive function.
            with path.open("r") as fil:
                cpr = configparser.ConfigParser(strict=True)
                return tuple((cpr.read_file(fil, source=str(path)),))

        except IOError as err:
            if required:
                raise ConfigError(
                    "Unable to read required configuration file {}".format(path)
                ) from err
            elif not default:
                current_app.logger.warning(
                    "Unable to read configuration file {}: {}".format(
                        path, err.strerror
                    )
                )


def _bool_get(value):
    """
    Return a boolean value, translating from other types if necessary.
    """

    if value.lower() not in BOOLEAN_VALUES:
        raise ValueError("Not a valid boolean: %s" % value)
    return BOOLEAN_VALUES[value.lower()]


def _get(gtype, section, key, required=False, default=None):
    """
    Get a configuration key.
    If required and it doesn't exist, raise an exception, otherwise return the default value.
    """

    name = (
        "{}_{}".format(section.upper(), key.upper())
        if section != "flask"
        else key.upper()
    )
    env_name = "HOTPOTATO_{}_{}".format(section.upper(), key.upper())

    if env_name in os.environ:
        return (name, gtype.env(os.environ.get(env_name)))
    elif required:
        try:
            return (name, gtype.file(section, key))
        except KeyError:
            raise ConfigError(
                "Required configuration parameter '{}' not found "
                "in the environment or in loaded configuration files".format(name)
            )
    else:
        return (name, gtype.file(section, key, fallback=default))
