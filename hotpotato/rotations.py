"""
Rotation functions.
"""


from hotpotato.models import Rotation as Model, db


class RotationError(Exception):
    """
    Rotation exception base class.
    """

    pass


class RotationIDError(RotationError):
    """
    Exception for invalid rotation ID.
    """

    def __init__(self, rotation_id):
        self.rotation_id = rotation_id
        super().__init__(
            "Invalid rotation ID (or unable to find rotation with ID): {}".format(
                rotation_id
            )
        )


class RotationNameError(RotationError):
    """
    Exception for invalid rotation name.
    """

    def __init__(self, rotname):
        self.rotname = rotname
        super().__init__(
            "Invalid rotation name (or unable to find rotation with name): {}".format(
                rotname
            )
        )


def get(rotation_id):
    """
    Get a rotation object using the given ID and return it.
    """

    rotation = Model.query.filter_by(id=rotation_id).first()

    if not rotation:
        raise RotationIDError(rotation_id)

    return rotation


def get_by_name(rotname):
    """
    Get a rotation object using its name and return it.
    """

    rotation = Model.query.filter_by(rotname=rotname).first()

    if not rotation:
        raise RotationNameError(rotname)

    return rotation


def create(rotname, person):
    """
    Create a rotation object using the given information, add it to the database, and return it.
    Parameters:
     * rotname - name of the rotation
     * person - user ID of the current on-call person for the rotation
    """

    rotation = Model(rotname=rotname, person=person)

    db.session.add(rotation)
    db.session.commit()

    return rotation
