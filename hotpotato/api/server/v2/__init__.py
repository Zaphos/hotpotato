"""
Server API version 2 blueprint and endpoints.
"""


from hotpotato.api.server.v2 import alerts  # noqa: F401
from hotpotato.api.server.v2 import errors  # noqa: F401
from hotpotato.api.server.v2 import heartbeats  # noqa: F401
from hotpotato.api.server.v2 import senders  # noqa: F401
from hotpotato.api.server.v2._blueprint import blueprint  # noqa: F401
