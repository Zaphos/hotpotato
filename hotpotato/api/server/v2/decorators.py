"""
Server API version 2 helper decorators.
"""


import functools

from hotpotato.api.server.v2 import (
    exceptions as api_server_exceptions,
    functions as api_server_functions,
)

__all__ = ["auth_required"]


def auth_required(func):
    """
    Decorator for making sure that an API endpoint can only be accessed if the server
    is authenticated.
    NOTE: This needs to be BELOW (inside) the Flask route decorator AND the @endpoint
          decorator.
    """

    # pylint: disable=missing-docstring
    @functools.wraps(func)
    def inner(*args, **kwargs):
        try:
            api_server_functions.server_get_from_auth()
            return func(*args, **kwargs)
        except api_server_exceptions.APIServerV2AuthError as err:
            return api_server_functions.api_json_response_get(
                success=err.success, code=err.code, message=str(err)
            )

    return inner
