"""
Web API version 1 message handler.
"""


from marshmallow import fields

from hotpotato.api.web.v1.notifications.handlers import (
    notifications as handler_notifications,
)
from hotpotato.notifications.messages import NOTIF_TYPE, get

__all__ = ["NOTIF_TYPE", "get", "schema"]


class MessageSchema(handler_notifications.NotificationSchema):
    from_user_id = fields.Int(required=True, json=True)
    reply_to_notif_id = fields.Int(required=True, json=True)
    message = fields.Str(required=True, json=True)


schema = MessageSchema()
