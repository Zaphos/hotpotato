"""
Web API version 1 helper functions.
"""


import copy
from http import HTTPStatus

# from hotpotato import servers
from hotpotato.api import functions as api_functions
from hotpotato.api.web.v1 import exceptions as api_web_exceptions

# import flask


# def server_get_from_auth():
#     '''
#     Find the object representing the server making the request using its auth info,
#     and return it.
#     '''
#
#     headers = flask.request.headers
#
#     if "Authorization" not in headers:
#         raise api_web_v1_exceptions.AuthHeaderError()
#
#     result = re.match("^([A-Za-z]*) (.*)$", headers.get("Authorization"))
#
#     method = result.group(1).lower()
#     if method != "apikey":
#         raise api_web_v1_exceptions.AuthMethodError(
#             actual_value=method,
#             possible_values=("apikey",),
#         )
#
#     api_key = result.group(2)
#     try:
#         return servers.get_by_api_key(api_key)
#     except servers.ServerAPIKeyError:
#         raise api_web_v1_exceptions.AuthAPIKeyError(api_key)


def api_json_response_get(
    message=None, data=None, success=True, code=HTTPStatus.OK, no_cache=False
):
    """
    Return a Web API version 1 standard JSON response.
    """

    if not isinstance(success, bool):
        raise api_web_exceptions.APIWebV1ResponseError(
            "Invalid value for success: got {}, expected bool".format(
                type(success).__name__
            ),
            success=False,
            code=HTTPStatus.INTERNAL_SERVER_ERROR,
        )
    if not isinstance(code, int):
        raise api_web_exceptions.APIWebV1ResponseError(
            "Invalid value for code: got {}, expected int".format(type(code).__name__),
            success=False,
            code=HTTPStatus.INTERNAL_SERVER_ERROR,
        )
    if message and not isinstance(message, str):
        raise api_web_exceptions.APIWebV1ResponseError(
            "Invalid value for message: got {}, expected str".format(
                type(message).__name__
            ),
            success=False,
            code=HTTPStatus.INTERNAL_SERVER_ERROR,
        )
    if data and not isinstance(data, dict):
        raise api_web_exceptions.APIWebV1ResponseError(
            "Invalid value for data: got {}, expected dict".format(type(data).__name__),
            success=False,
            code=HTTPStatus.INTERNAL_SERVER_ERROR,
        )

    return api_functions.json_response_get(
        {
            "success": success,
            "code": code,
            "message": message,
            "data": copy.deepcopy(data) if data else {},
        },
        code=code,
        no_cache=no_cache,
    )
