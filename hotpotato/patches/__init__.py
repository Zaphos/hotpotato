"""
Monkey patch helper classes and methods.
"""


import distutils.version

version = distutils.version.StrictVersion
