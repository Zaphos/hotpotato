"""
Flask-Security monkey patch.

This is to work around a bug that has been fixed upstream,
but not incorporated into the latest release (as of this file, 3.0.0).

The monkey patch does not get applied to versions of Flask-Security newer than 3.0.0.
"""


from flask_security import __version__ as flask_security_version

from hotpotato import patches

if patches.version(flask_security_version) <= patches.version("3.0.0"):
    from flask import current_app
    from flask import render_template

    from flask_mail import Message

    from flask_security import changeable
    from flask_security import confirmable
    from flask_security import passwordless
    from flask_security import recoverable
    from flask_security import registerable
    from flask_security import utils

    from flask_security.utils import _security
    from flask_security.utils import config_value

    from werkzeug.local import LocalProxy

    def send_mail(subject, recipient, template, **context):
        """Send an email via the Flask-Mail extension.

        :param subject: Email subject
        :param recipient: Email recipient
        :param template: The name of the email template
        :param context: The context to render the template with
        """

        context.setdefault("security", _security)
        context.update(_security._run_ctx_processor("mail"))

        sender = _security.email_sender
        if isinstance(sender, LocalProxy):
            sender = sender._get_current_object()

        msg = Message(subject, sender=sender, recipients=[recipient])

        ctx = ("security/email", template)
        if config_value("EMAIL_PLAINTEXT"):
            msg.body = render_template("%s/%s.txt" % ctx, **context)
        if config_value("EMAIL_HTML"):
            msg.html = render_template("%s/%s.html" % ctx, **context)

        if _security._send_mail_task:
            _security._send_mail_task(msg)
            return

        mail = current_app.extensions.get("mail")
        mail.send(msg)

    utils.send_mail = send_mail
    registerable.send_mail = send_mail
    passwordless.send_mail = send_mail
    recoverable.send_mail = send_mail
    changeable.send_mail = send_mail
    confirmable.send_mail = send_mail
