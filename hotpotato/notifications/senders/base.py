"""
Notification sender base class.
"""


from hotpotato import oncall_contacts


class BaseSender:
    """
    Notification sender base class.
    """

    name = "base"
    description = "base sender (SHOULD NOT BE USED)"

    method = "base"
    provider = "base"

    def send(self, notif, contact):
        """
        Send a notification to a contact.
        """

        raise NotImplementedError()

    def can_send(self, user_id):
        """
        Return True if the user with the given ID can use this sender.
        """

        return oncall_contacts.exists(user_id=user_id, method=self.method)
