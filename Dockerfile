FROM node:11

RUN mkdir /theme
WORKDIR /theme
COPY ["theme/package.json", "theme/package-lock.json", "/theme/"]
RUN npm i
COPY ["theme/src", "theme/gulpfile.js", "/theme/"]
RUN npm run build


FROM python:3.6

ARG COCKROACH_LOCAL
ARG RABBITMQ_LOCAL
ARG HOTPOTATO_BUILD_DEV

ENV HOTPOTATO_LOG_DIR="${HOTPOTATO_LOG_DIR:-/var/log/hotpotato}"
ENV FLASK_APP="hotpotato.app.main:app"

ENV FLASK_DEBUG="${FLASK_DEBUG:-false}"

# NOTE: If COCKROACH_SERVER is set to something other than "localhost"
# or the local loopback address, it will be assumed that a remote server
# is being used, and CockroachDB will NOT be installed.
ENV COCKROACH_VERSION="${COCKROACH_VERSION:-2.1.3}"
ENV HOTPOTATO_COCKROACH_SERVER="${HOTPOTATO_COCKROACH_SERVER:-localhost}"
ENV HOTPOTATO_COCKROACH_PORT="${HOTPOTATO_COCKROACH_PORT:-26257}"
ENV HOTPOTATO_COCKROACH_DATABASE="${HOTPOTATO_COCKROACH_DATABASE:-hotpotato}"
# HOTPOTATO_COCKROACH_USERNAME is undefined
# HOTPOTATO_COCKROACH_PASWORD is undefined

# NOTE: If HOTPOTATO_RABBITMQ_SERVER is set to something other than "localhost"
# or the local loopback address, it will be assumed that a remote server
# is being used, and RabbitMQ will NOT be installed.
ENV HOTPOTATO_RABBITMQ_SERVER="${HOTPOTATO_RABBITMQ_SERVER:-127.0.0.1}"
ENV HOTPOTATO_RABBITMQ_PORT="${HOTPOTATO_RABBITMQ_PORT:-5672}"
ENV HOTPOTATO_RABBITMQ_USE_SSL="${HOTPOTATO_RABBITMQ_USE_SSL:-false}"
ENV HOTPOTATO_RABBITMQ_USERNAME="${HOTPOTATO_RABBITMQ_USERNAME:-hotpotato}"
ENV HOTPOTATO_RABBITMQ_PASSWORD="${HOTPOTATO_RABBITMQ_PASSWORD:-hotpotato}"
ENV HOTPOTATO_RABBITMQ_VHOST="${HOTPOTATO_RABBITMQ_VHOST:-/}"

RUN mkdir -p /code
RUN mkdir -p "${HOTPOTATO_LOG_DIR}"

COPY ["docker/hotpotato/install-cockroach", "/install-cockroach"]
COPY ["docker/hotpotato/install-rabbitmq", "/install-rabbitmq"]
COPY ["docker/hotpotato/install-hotpotato", "/install-hotpotato"]
COPY ["docker/hotpotato/install", "/install"]
COPY ["docker/hotpotato/start-cockroach", "/start-cockroach"]
COPY ["docker/hotpotato/start-rabbitmq", "/start-rabbitmq"]
COPY ["docker/hotpotato/start-hotpotato", "/start-hotpotato"]
COPY ["docker/hotpotato/start", "/start"]

RUN chmod +x /install-cockroach /install-rabbitmq /install-hotpotato /install /start-cockroach /start-rabbitmq /start-hotpotato /start

COPY [".", "/code"]
COPY --from=0 ["/theme/dist", "/code/theme/dist"]

RUN /install

ENTRYPOINT ["dumb-init", "/start"]

EXPOSE 8000
