*** Settings ***
Documentation     Validate the Servers page.
Resource          resources/common.robot
Suite Setup       Open Browser To Login Page
Suite Teardown    Close Browser
Test Teardown     Logout


*** Test Cases ***
Open Servers Page
    Login
    Go To Servers Page
    Servers Page Should Be Open
