// Initialise BS Tooltip
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

$(document).ready(function() {
  $('[data-toggle="tooltip"]').tooltip({
    animation: false
  });
});

// Adding/removing focus state class for BS preppend/append inputs (global)
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
$(document).ready(function() {
  $(".form-control").focus(function() {
    $(this).prev('.input-group-prepend').addClass('input-group-prepend-focus');
    $(this).next('.input-group-append').addClass('input-group-append-focus');
  });
  $(".form-control").focusout(function() { // Removing focus class
    $(this).prev('.input-group-prepend-focus').removeClass('input-group-prepend-focus');
    $(this).next('.input-group-append-focus').removeClass('input-group-append-focus');
  });
});
// Servers notifications enable/disable toggler
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

$(document).ready(function() {
  $('.alert-checkbox').change(function() {
    $(this).closest(".list-group-item").toggleClass("disabled");
  });
});

// Notifications action confirmation messages
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
$(document).ready(function() {
  $(".link-copy").click(function() {
    $(this).closest(".card").addClass("link-copied");
  });
});


$(document).ready(function() {
  $(".link-copy").click(function() {
    $(this).closest(".card").addClass("copy-confirm");
    setTimeout(RemoveClass, 2000);
  });

  function RemoveClass() {
    $('.card').removeClass("copy-confirm");
  }
});


// 'Tater button' animation (via anime.js)
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

$(document).ready(function() {
  var pathBtn = 'M 62.05 56 C 43.967 56 25.883 56 7.8 56 C 3.5 56 0 52.5 0 48.3 C 0 37.433 0 26.567 0 15.7 C 0 11.5 3.5 8 7.8 8 C 16.4 8 25 8 33.6 8 C 42.2 8 50.8 8 59.4 8 C 78.333 8 97.267 8 116.2 8 C 116.2 8 116.2 8 116.2 8 C 120.5 8 124 11.5 124.1 15.7 C 124.1 26.533 124.1 37.367 124.1 48.2 C 124.1 52.5 120.6 56 116.3 56 C 107.258 56 98.217 56 89.175 56 L 62.05 56'
  var pathTater = 'M 63.7 68 C 57.3 68.1 50.9 67.4 44.7 66 C 30.9 62.7 18.3 54.1 9.4 42.8 C 4.6 36.7 -0.5 29 0 20.8 C 0.5 12.4 7.1 6.6 14.3 3.9 C 22.2 1 29.2 2 37.1 3.6 C 47.5 5.7 52.8 7.2 62.8 3.7 C 73.3 0.05 83.075 -0.475 90.975 0.338 C 98.875 1.15 104.9 3.3 107.9 5 C 114.2 8.6 119.7 14 122.4 20.9 C 124.7 26.75 124.45 32.4 122.5 37.5 C 120.55 42.6 116.9 47.15 112.4 50.8 C 98.6 61.9 81.2 67.8 63.7 68 L 63.7 68'

  var fillBtn = '#F5512A'
  var fillTater = '#895063'

  function taterAnim(el, path, fill) { // Declare anime.js animated properties
    anime.remove(el);
    anime({
      targets: el,
      d: path,
      easing: 'easeOutElastic', //easeOutElastic
      duration: 1000,
      fill: fill
    });
  };

  function transformToTater(el, path) { // Define svg paths for each end of anim
    taterAnim(el, pathTater, fillTater);
  };

  function transformToBtn(el, path) {
    taterAnim(el, pathBtn, fillBtn);
  };

  $('.btn-tater').each(function() { // Find tater - bind events
    $(this).on('mouseover', function() {
      transformToTater($(this).find('#btn-tater').get(0));
    });
    $(this).on('mouseout', function() {
      transformToBtn($(this).find('#btn-tater').get(0));
    });
  });
});

// Auto focus modal inputs
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

$(document).ready(function() {
  $('.modal').on('shown.bs.modal', function() {
    $('.input-focused').trigger('focus');
  });
});
